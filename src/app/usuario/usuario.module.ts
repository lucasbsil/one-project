import { UsuarioComponent } from './usuario.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [UsuarioComponent],
  imports: [
    UsuarioModule,
    CommonModule
  ]
})
export class UsuarioModule { }
