import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from  '@angular/router';
import { UsuarioComponent } from './usuario/usuario.component';
import { HomeComponent } from './home/home.component';

const appRoutes: Routes = [
  { path: 'home',
    loadChildren: './home.module#HomeModule'
},
{ path: 'usuario',
loadChildren: './usuario.module#UsuarioModule'
},
{
  path: '',
  pathMatch: 'full',
  redirectTo: '/home'
}
];


@NgModule({
  declarations: [],
  imports: [
   RouterModule.forRoot(appRoutes, {useHash: true})
  ],
   exports: [RouterModule]
})
export class AppRoutingModule { }
